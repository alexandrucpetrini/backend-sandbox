<?php

class Node
{

    public static $nodes;

    public function __construct($code)
    {
        self::$nodes[$code] = $this;
    }

    protected function getNext($code)
    {

        if (isset(self::$nodes[$code])) {
            return self::$nodes[$code];
        }
        return new DummyNode();
    }
}

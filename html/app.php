<?php

include "lib/Node.php";
include "lib/DummyNode.php";

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

class app
{
    private $MAX = 20;

    public $nodes;
    public $codes;

    public function __construct()
    {
        foreach (glob("nodes/*.php") as $i => $path) {
            include $path;
            $basename = basename($path, ".php");
            $instance = new $basename();
            $this->nodes[] = $instance;
        }
    }

    public function getImage($path)
    {
        $data = file_get_contents($path);

        $parts = str_split($data, round(strlen($data) / $this->MAX));
        $final = [];

        foreach ($parts as $i => $part) {
            if ($i <= count($this->nodes) - 1) {
                $node = $this->nodes[$i];
                $final[$node->CODE] = base64_encode($part);
            }
        }

        return $final;
    }

    public function send($data)
    {
        $first = current($this->nodes);
        $first->go($data);
    }
}

<?php

include_once "lib/Watermark.php";

class NodeTestOne4AdrianAdiaconitei extends Node
{
    public $CODE = '6833D6E78B86C043452530FA69D6DA2D';
    public $NAME = 'Adrian Adiaconitei';

    public function __construct()
    {
        parent::__construct($this->CODE);
    }

    /**
     * Functia asta poate avea orice nume
     *
     * @param [type] $message
     * @return void
     */
    public function sendFurther($message)
    {
        $next = $this->getNext('17442E8144DFB61C7795E472E534F286');
        $data = base64_decode($message['6833D6E78B86C043452530FA69D6DA2D']);
        // echo $data;
        ($watermak = new Watermark($this->NAME, "#003398", $message, $next))->showImage();
        $next->sendNextColleague($message);
    }
}
<?php

include_once "lib/Watermark.php";
class NodeTestOne2StoicaMircea extends Node
{
    public $CODE = '53BF2A679D01B8AB9F23C1D6EEB11554';
    public $NAME = 'Mircea Stoica';

    public function __construct()
    {
        parent::__construct($this->CODE);
    }

    /**
     * Functia asta poate avea orice nume
     *
     * @param [type] $message
     * @return void
     */
    public function send($message)
    {
        $next = $this->getNext('DCF813D7A9AEF41BADD01CE82027328C');
        $data = base64_decode($message['53BF2A679D01B8AB9F23C1D6EEB11554']);
        // echo $data;
        ($watermak = new Watermark($this->NAME, "#00F033", $message, $next))->showImage();
        $next->printimg($message);
    }
}
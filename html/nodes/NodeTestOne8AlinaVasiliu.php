<?php

include_once "lib/Watermark.php";

class NodeTestOne8AlinaVasiliu extends Node
{

    public $CODE = 'FF6F7AE1F522E4067BC5947D8F56953C';
    public $NAME = 'Alina Vasiliu';
    public $NEXT_CODE = '70CB5184648F1B966F77324393A6F878';

    public function __construct()
    {
        parent::__construct($this->CODE);
    }

    public function goaway($message)
    {
        $next = $this->getNext($this->NEXT_CODE);
        $data = base64_decode($message[$this->CODE]);

        // echo $data;
        ($watermak = new Watermark($this->NAME, "#449911", $message, $next))->showImage();
        $next->nextnextnext($message);
    }

}
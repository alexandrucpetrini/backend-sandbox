<?php

require_once "lib/Watermark.php";
class NodeTestOne3AlexandruPetrini extends Node
{
    public $CODE = 'DCF813D7A9AEF41BADD01CE82027328C';
    public $NAME = 'Alexandru Petrini';
    public $NEXT_CODE = '6833D6E78B86C043452530FA69D6DA2D';

    public function __construct()
    {
        parent::__construct($this->CODE);
    }

    /**
     * Functia asta poate avea orice nume
     *
     * @param [type] $message
     * @return void
     */
    public function printimg($message)
    {
        $next = $this->getNext($this->NEXT_CODE);
        $data = base64_decode($message[$this->CODE]);
        // echo $data;
        ($watermak = new Watermark($this->NAME, "#F030FF", $message, $next))->showImage();
        $next->sendFurther($message);
    }
}
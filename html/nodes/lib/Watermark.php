<?php

class Watermark
{
    const ALPHA = 80; //  transparency in procents
    const MAX = 20; // max users

    /**
     * $names - stores node names
     *
     * @var array
     */
    private static $names = [];

    /**
     * $image - resource from the $message array
     *
     * @var resource|false
     */
    private $image;

    /**
     * $message - array that holds base 64 coded parts of the image
     * but not the entire image.
     *
     * @var array
     */
    private $message;

    /**
     * $count - number of nodes
     *
     * @var undefined
     */
    private $count;

    /**
     * $colors - array of integers holding
     * each user's color. The colors are just ready to be used
     *
     * @var array
     */
    private static $colors = [];

    private $imageWidth;
    private $imageHeight;
    private $nextNode;

    public function __construct($name, $hexColor, $message, $next)
    {
        self::$names[] = $name;
        $this->count = count(self::$names);
        $this->message = $message;
        $this->image = $this->getImage($message);
        self::$colors[] = $this->getColor($hexColor);
        $this->imageWidth = imagesx($this->image);
        $this->imageHeight = imagesy($this->image);
        $this->nextNode = $next;
    }

    /**
     * getImage
     *
     * @param mixed $message
     * @return resource|false
     */
    public function getImage($message)
    {
        $imageContent = '';
        foreach ($message as $code => $part) {
            $imageContent .= base64_decode($part);
        }
        return imagecreatefromstring($imageContent);
    }

    /**
     * getColor - returns the int value of $hexcolor
     * so that it can be used directly in imagefilledrectangle
     *
     * @param string $hexColor
     * @return int|false
     */
    private function getColor($hexColor)
    {
        // 127 means fully transparent
        $alpha = round(127 * self::ALPHA / 100);
        $hexColor = ltrim($hexColor, '#');
        $redValue = hexdec(substr($hexColor, 0, 2));
        $greenValue = hexdec(substr($hexColor, 2, 2));
        $blueValue = hexdec(substr($hexColor, 4, 2));
        return imagecolorallocatealpha(
            $this->image,
            $redValue,
            $greenValue,
            $blueValue,
            $alpha
        );
    }

    /**
     * showImage: builds and displays image
     *
     * @return void
     */
    public function showImage()
    {
        // if the next node is not DummyNode (the last)
        // we return because we cannot overwrite the image
        // displayed  by the presious node
        if ($this->nextNode instanceof Node) {
            return;
        }

        // It's the last Node, so we start creating the image
        $rectangleHeight = round($this->imageHeight / self::MAX);

        // Crop Image to make make it look nice
        // If we don't crop it we'll have an unfinished image line
        // comment next line to see what I mean
        $cropImageHeight = $this->count * $rectangleHeight;
        $this->image = imagecrop($this->image, ['x' => 0, 'y' => 0, 'width' => $this->imageWidth, 'height' => $cropImageHeight]);

        // create collored rectangles with text on image
        $x1 = 0;
        $x2 = $this->imageWidth;
        $y1 = 0;
        $y2 = $rectangleHeight;
        $white = imagecolorallocate($this->image, 255, 255, 255); // text color
        $xOffset = round($this->imageWidth * 5 / 100); // 5% offset from left
        $yOffset = round($rectangleHeight / 3); // a third of the rectancle height offset from top
        for ($i = 0; $i < $this->count; $i++) {
            imagefilledrectangle($this->image, $x1, $y1, $x2, $y2, self::$colors[$i]);
            imagestring($this->image, 5, $x1 + $xOffset, $y1 + $yOffset, self::$names[$i], $white);
            $y1 = $y2 + 1; // +1 (pixel), just so we don't overlap rectangles
            $y2 += $rectangleHeight;
        }

        // No need for header here as it's present in image.php
        imagepng($this->image);
        imagedestroy($this->image);
    }
}
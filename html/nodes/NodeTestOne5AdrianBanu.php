<?php

include_once "lib/Watermark.php";

class NodeTestOne5AdrianBanu extends Node
{
    /**
     *Already used:
     *57B5EBEF5E6F97D85C0F808F71817355
     *53BF2A679D01B8AB9F23C1D6EEB11554
     *DCF813D7A9AEF41BADD01CE82027328C
     *6833D6E78B86C043452530FA69D6DA2D
     */

    public $CODE = '17442E8144DFB61C7795E472E534F286';
    public $NAME = 'Adrian Banu';

    public function __construct()
    {
        parent::__construct($this->CODE);
    }

    /**
     * Functia asta poate avea orice nume
     *
     * @param [type] $message
     * @return void
     */
    public function sendNextColleague($message)
    {
        $next = $this->getNext('A8F5F167F44F4964E6C998DEE827110C');
        $data = base64_decode($message['17442E8144DFB61C7795E472E534F286']);
        // echo $data;
        ($watermak = new Watermark($this->NAME, "#000033", $message, $next))->showImage();
        $next->nextCode($message);
    }
}
<?php

include_once "lib/Watermark.php";

class NodeTestOne7StancuBogdan extends Node
{

    public $CODE = 'A221E37FC8D862F436F40750E8A9438C';
    public $NAME = 'Stancu Bogdan';
    public $NEXT_CODE = 'FF6F7AE1F522E4067BC5947D8F56953C';

    public function __construct()
    {
        parent::__construct($this->CODE);
    }

    /**
     * Functia asta poate avea orice nume
     *
     * @param [type] $message
     * @return void
     */
    public function pushdown($message)
    {
        $next = $this->getNext($this->NEXT_CODE);
        $data = base64_decode($message[$this->CODE]);
        // echo $data;
        ($watermak = new Watermark($this->NAME, "#000033", $message, $next))->showImage();
        $next->goaway($message);
    }

}
<?php

include_once "lib/Watermark.php";

class NodeTestOne6AurelianNicolaica extends Node
{
    public $CODE = 'A8F5F167F44F4964E6C998DEE827110C';
    public $NAME = 'Aurelian Nicolaica';

    public function __construct()
    {
        parent::__construct($this->CODE);
    }

    /**
     * Functia asta poate avea orice nume
     *
     * @param [type] $message
     * @return void
     */
    public function nextCode($message)
    {
        $next = $this->getNext('A221E37FC8D862F436F40750E8A9438C');
        $data = base64_decode($message[$this->CODE]);

        /*
        $img = imagecreatefromstring($data);

        $w = imagesx($img);
        $h = imagesy($img);

        $img2 = imagecreatetruecolor($w, $h);
        imagesavealpha($img2, true);

        $color = imagecolorallocate($img2, 255, 0, 0);
        imagefilledrectangle($img2, 0, 0, $w, $h, $color);

        imagecopymerge($img, $img2, 0, 0, 0, 0, $w, $h, 80);

        ob_start();
        imagepng($img);
        $data = ob_get_clean();

        imagedestroy($img);
        imagedestroy($img2);
         */
        // echo($data);
        ($watermak = new Watermark($this->NAME, "#000033", $message, $next))->showImage();
        $next->pushdown($message);
    }
}

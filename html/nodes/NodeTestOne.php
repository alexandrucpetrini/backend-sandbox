<?php
include_once "lib/Watermark.php";

class NodeTestOne extends Node
{
    public $CODE = '57B5EBEF5E6F97D85C0F808F71817355';
    public $NAME = 'Alexandru Neacsu';

    public function __construct()
    {
        parent::__construct($this->CODE);
    }

    /**
     * Functia asta poate avea orice nume
     *
     * @param [type] $message
     * @return void
     */
    public function go($message)
    {
        $next = $this->getNext('53BF2A679D01B8AB9F23C1D6EEB11554');
        $data = base64_decode($message['57B5EBEF5E6F97D85C0F808F71817355']);
        // echo $data;
        ($watermak = new Watermark($this->NAME, "#3300FF", $message, $next))->showImage();
        $next->send($message);
    }
}
<?php include "app.php";
$app = new app();?>
<!DOCTYPE html>
<html>

<head>

    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
    <link rel="icon" href="https://digitalnation.ro/wp-content/uploads/2018/07/cropped-transparent_half_icon-32x32.png"
        sizes="32x32">
    <link rel="icon"
        href="https://digitalnation.ro/wp-content/uploads/2018/07/cropped-transparent_half_icon-192x192.png"
        sizes="192x192">

    <style>
    body {
        font-family: 'Source Sans Pro', sans-serif;
        background-color: #f5f5f5;
        margin: 0;
        padding: 0;
    }

    h1 {
        border-bottom: 1px dashed #dcdcdc;
        padding-bottom: 20px;
    }

    .container {
        width: 80%;
        text-align: center;
        background: #fff;
        border-radius: 10px;
        padding: 5px;
        margin: 20px auto;
        max-width: 900px;
        margin-bottom: 50px;
    }

    .students {
        list-style: decimal;
        text-align: left;
    }

    .image {
        background: #000;
        background-image: url(./res/gl.jpg);
        border: 2px solid #eaeaea;
        border-radius: 5px;
        max-width: 99%;
    }
    </style>
</head>

<body>

    <div class="container">

        <h1>Digital Nation Backend Sandbox</h1>

        <ul class="students">
            <?php foreach ($app->nodes as $node): ?>
            <li><?=$node->NAME?>
            </li>
            <?php endforeach;?>
        </ul>

        <img class="image" src="./image.php?rand=<?=rand(0, 1000)?>" />

        <script>
        document.write('<script src="http://' +
            (location.host || 'localhost').split(':')[0] +
            ': 35729/livereload.js"></' +
            'script>')
        </script>
    </div>

</body>

</html>